﻿using System;
using System.Collections.Generic;

namespace ChessKnight
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        
        int chessKnight(string cell) {
            int maxMoves = 8;
            
            if(cell[0] == 'b' || cell[0] == 'g'){
                maxMoves -= 2;
            }
            if(cell[1] == '2' || cell[1] == '7'){
                maxMoves -= 2;
            }
            if(cell[0] == 'a' || cell[0] == 'h'){
                maxMoves /= 2;
            }
            if(cell[1] == '1' || cell[1] == '8'){
                maxMoves /= 2;
            }
            
            
            return maxMoves;
        }


    }
}
